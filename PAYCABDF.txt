      DELETE   (PPSP.PCICSV.PAYCABY)   CLUSTER
     DEFINE        -
         CLUSTER(          -
                 NAME(PPSP.PCICSV.PAYCABY)      -
                 VOL(PRDATA)                 -
                 KEYS(21 0)                  -
                 RECSZ(143 143)              -
                 FREESPACE(10 20)             -
                 IMBED                       -
                 SHR(2 3)                    -
                 SPEED                       -
                 UNIQUE                      -
                 INDEXED                     -
                 )                           -
         INDEX(            -
               NAME(PPSP.PCICSV.PAYCABY.INDX)   -
               CISZ(4096)                    -
               )                             -
         DATA(             -
               NAME(PPSP.PCICSV.PAYCABY.DATA)   -
               CISZ(4096)                    -
               CYL(1 0)                    -
               )
