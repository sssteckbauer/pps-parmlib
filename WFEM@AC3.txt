send
     account(WFBD) userid(WFBD004)  #Wells Fargo El Monte
     fileid(DD:UCSDOUT)             #send the file from DD UCSDOUT
     mode( )                        #normal mode
     class(KH012)                   #class to identify XX-CYCLE file
     charge(3)                      #who pays for transmission?
     ack(f)                         #generate all acknowledgments
     verify(y)                      #make sure dest mailbox is valid
     retain(14);                    #retain for 14 days or until recv'd
