ATP01HEALTH NET DEDUCTIONS/CONTRIBUTIONS                                00010000
DRP01062064                                            SCI  SN          00020000
ATP02KAISER NORTH DEDUCTIONS/CONTRIBUTIONS                              00030000
DRP02047049                                            SCI  SN          00040000
ATP03KAISER SOUTH DEDUCTIONS/CONTRIBUTIONS                              00050000
DRP03048052                                            SCI  SN          00060000
ATP04PRUDENTIAL HEALTH DEDUCTIONS/CONTRIBUTIONS                         00070000
DRP04045057                                            SCI  SN          00080000
ATP05HIGH OPTION (BLUE CROSS)                                           00080100
DRP05306307                                            SCI  SN          00080200
ATP06CORE-MEDICAL DEDUCTIONS/CONTRIBUTIONS                              00080300
DRP06206207                                            SCI  SN          00080400
ATP07CORE (BLUE CROSS)                                                  00080500
DRP07308309                                            SCI  SN          00080600
ATP08PACIFIC CARE DEDUCTIONS/CONTRIBUTIONS                              00080700
DRP08220221                                            SCI  SN          00080800
ATP09UC CARE POS DEDUCTIONS/CONTRIBUTIONS                               00080900
DRP09274275                                            SCI  SN          00081000
ATP10BLUE CROSS PLUS                                                    00082000
DRP10302303                                            SCI  SN          00083000
ATP11BLUE CROSS PPO                                                     00084000
DRP11304305                                            SCI  SN          00085000
ATP12FHP/TAKECARE                                                       00086000
DRP12144145                                            SCI  SN          00086100
ATP13FOUNDATION HEALTH                                                  00086200
DRP13044051                                            SCI  SN          00086300
ATP14FORFEIT INSURANCE                                                  00086400
DRP14254                                               SCI  SN          00086500
ATP15LIFE INSURANCE DEDUCTIONS                                          00086600
DRP15050                                               SCI  SN          00086700
ATP16LIFE-NO TIP                                                        00086800
DRP16255                                               SCI  SN          00086900
ATP17DEPENDENT LIFE INSURANCE DEDUCTIONS                                00087000
DRP17043                                               SCI  SN          00088000
ATP18CDS DENTAL DEDUCTIONS/CONTRIBUTIONS                                00089000
DRP18065066                                            SCI  SN          00090000
ATP19PMI-DENTAL DEDUCTIONS/CONTRIBUTIONS                                00100000
DRP19244245                                            SCI  SN          00110000
ATP20AD&D INSURANCE DEDUCTIONS/CONTRIBUTIONS                            00120000
DRP20040                                               SCI  SN          00130000
ATP21AD&D-NO TIP                                                        00140000
DRP21256                                               SCI  SN          00150000
ATP22VISION CARE DEDUCTIONS/CONTRIBUTIONS                               00160000
DRP22240241                                            SCI  SN          00170000
ATP23LEGAL SERVICE DEDUCTIONS/CONTRIBUTIONS                             00180000
DRP23157158                                            SCI  SN          00190000
ATP24ARAG LEGAL SERVICE DEDUCTIONS/CONTRIBUTIONS                        00200000
DRP24340341                                            SCI  SN          00210000
