 DSN SYSTEM(DB2P)                                                       00760000
 BIND PLAN(C1PPOT53)   +                                                00770000
      OWNER(PPPDBC1)   +                                                00780000
      MEMBER(PPMSSG2,  +                                                00790000
             PPFAU018, +                                                00800000
             PPFAU035, +                                                00810000
             PPLINIFS, +                                                00820000
             PPCTTUTL) +                                                00830000
      ACT(REPLACE) RETAIN  +                                            00840000
      VALIDATE(BIND) +                                                  00850000
      ISOLATION(CS)   +                                                 00860000
      FLAG(I)   +                                                       00870000
      ACQUIRE(USE)   +                                                  00880000
      RELEASE(COMMIT) +                                                 00890000
      EXPLAIN(NO)                                                       00900000
