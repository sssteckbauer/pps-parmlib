 09SORT ID-NUMBER-TRUNCATED                                             00010000
 090 RECORDS-WRITTEN-COUNTER 1                                          00020000
 093 UCSD EMPS WITH INCOMPATIBLE FICA CODING AND AMTS                   00030000
 0951*  0 RECORDS-WRITTEN-COUNTER                                       00040000
 0951*  10ID-NUMBER-TRUNCATED     HH 'EMPL #'                           00050000
 0951*  20NAME-TRUNCATED          HH 'NAME'                             00060000
 0951*  30SOC-SEC-NR              FS HH 'SOC SEC #'                     00070000
 0951*  40FICA-ELIG-CDE           HH 'ELIG'                             00080000
 0951*  50YTD-TTL-GROSS           HH 'TOTAL GROSS'                      00090000
 0951*  60FICA-YTD-GRS            HH 'FICA GROSS'                       00100000
 0951*  70FICA-ONLY-WITHHELD      HH 'FICA WITHHELD'                    00110000
 0961   1-'TOTAL RECORDS ='                                             00120000
 0961  16-RECORDS-WRITTEN-COUNTER F- SZ=5                               00130000
 097010IF (((FICA-YTD-GRS > 0.00 OR FICA-ONLY-WITHHELD > 0.00) AND      00140000
*            FICA-ELIG-CDE NE 'E')                                      00150000
*          OR                                                           00160000
*          ((FICA-YTD-GRS = 0.00 OR FICA-ONLY-WITHHELD = 0.00) AND      00170000
*            FICA-ELIG-CDE = 'E')) TAKE                                 00180000
 097070DROP                                                             00190000
 09OUT                                                                  00200000
